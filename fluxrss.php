<?php 
	include 'db_connection.php';
 ?>
<!DOCTYPE html>
<html lang="fr">

<head>
	<title>Feedly</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/styleflux.css">
</head>

<body>
<script src="./js/script.js"></script>
