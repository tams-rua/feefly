
<?php include "header.php" ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-1" >
            <ul id="nav-toggle" style="z-index:999;">
                <li class="active"><a href="home.php">Home</a></li>
                <li><a href="#classement">Classement</a></li>
                <li><a href="#catégorie">Catégorie</a></li>
                <li><a href="#environnement">Environnement</a></li>
                <li><a href="#nechnologie">Technologie</a></li>
                <li><a href="#numérique">Numérique</a></li>
                <li><a class="contact" href="#contact">Contact</a></li>
            </ul>
            <section id="nav-trigger" style="background-color: rgba(27, 27, 27, 0.788);">
                    <i class="fas fa-bars" id="tab-toggle-nav" style="z-index:999; "></i>
            </section>
            <script>
                $(document).ready(function(){
                $('#tab-toggle-nav').click(function(){
                $('#nav-toggle').toggleClass('ul_show');
                $('#nav-trigger').toggleClass('slide_image');
                });
                $('li').click(function(){
                $(this).addClass('active').siblings().removeClass('active');
                });
            });
            </script>
        </div>
        <div style="position: absolute;
                top: 0;
                right: 10%;
                z-index:999;
                background-color: rgba(27, 27, 27, 0.788);">
            <i class="far fa-user fa-2x" style="color:blue"></i>
            </div> 
        <div class="col-12 content-aside" style="margin-top:100px; z-index:1;">
            <div class="row">
            <?php
                $url = "https://www.radio1.pf/feed/atom/"; /* insérer ici l'adresse du flux RSS de votre choix */
                $rss = simplexml_load_file($url);

                foreach ($rss->entry as $item):
                    $datetime = date_create($item->published);
                    $date = date_format($datetime, 'd M Y, H\hi');
                    echo '<div class="card  col-xs-12 col-md-5 m-5">';
                    echo    '<div class="card-header">';
                    echo        '<h6><a href="'.$item->link['href'].'">'.utf8_decode($item->title).'</a> ('.$date.')</h6>';
                    echo    '</div>';
                    echo    '<div class="card-body overflowCard" style="height:450px; overflow-y:scroll;">';
                    echo        '<div style="overflow:hidden">'.$item->content.'</div>';
                    echo    '</div>';
                    echo '</div>';
                endforeach;

                ?>
            </div>
        </div>
    </div>
</div>



<?php include "footer.php"; ?>